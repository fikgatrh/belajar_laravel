<?php

namespace App\Http\Controllers;

use App\Models\Transaction;
use Illuminate\Database\QueryException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpFoundation\Response;
use App\Helper\ResponseCustom;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $perPage = request('perPage'); // ->> query param untuk limit/per page
        $res = DB::table('transactions')->orderBy('updated_at', 'DESC')->paginate($perPage);
        $page = $perPage;

        if (is_null($res->previousPageUrl())) {
            $prevPage = $res->previousPageUrl();
        } else {
            $prevPage = $res->previousPageUrl() . '&perPage=' . $page;
        }

        if (is_null($res->nextPageUrl())) {
            $nexPage = $res->nextPageUrl();
        } else {
            $nexPage = $res->nextPageUrl() . '&perPage=' . $page;
        }

        $response = [
            'total_data' => $res->total(),
            'per_page' => $res->perPage(),
            'current_page' => $res->currentPage(),
            'last_page' => $res->lastPage(),
            'next_page_url' => $nexPage,
            'prev_page_url' => $prevPage,
            'from' => $res->firstItem(),
            'to' => $res->lastItem(),
            'data' => $res->items()
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage. -> untuk membuat data baru di suatu penyimpanan
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'amount' => ['required', 'numeric'],
            'type' => ['required', 'in:expense,revenue']
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $transaction = Transaction::create($request->all());
            $response = [
                'message' => 'test test',
                'data' => $transaction
            ];

            return response()->json($response, Response::HTTP_CREATED);

        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed' . $e->errorInfo
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show($id)
    {
        $transaction = Transaction::find($id);

        if (is_null($transaction)) {
            Log::channel('customLog')->error('[TransactionController.GetByID] Data not found');
            $resp = ResponseCustom::errorResp('Data not found');
            return $resp;
        }

        $response = [
            'message' => 'Detail transaction',
            'data' => $transaction,
        ];

        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $transaction = Transaction::find($id);

        if (is_null($transaction)) {
            Log::channel('customLog')->error('[TransactionController.UpdateData] Data not found');
            $resp = ResponseCustom::errorResp('Data not found');
            return $resp;
        }

        $validator = Validator::make($request->all(), [
            'title' => ['required'],
            'amount' => ['required', 'numeric'],
            'type' => ['required', 'in:expense,revenue']
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        try {
            $transaction->update($request->all());
            $response = [
                'message' => 'success updated',
                'data' => $transaction
            ];

            return response()->json($response, Response::HTTP_OK);

        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed' . $e->errorInfo
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $transaction = Transaction::find($id);

        if (is_null($transaction)) {
            $resp = ResponseCustom::errorResp('Data not found');
            return $resp;
        }

        try {
            $transaction->delete();
            $response = [
                'message' => 'success deleted',
            ];

            return response()->json($response, Response::HTTP_OK);

        } catch (QueryException $e) {
            return response()->json([
                'message' => 'Failed' . $e->errorInfo
            ]);
        }
    }
}
