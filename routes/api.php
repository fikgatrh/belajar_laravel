<?php

use App\Http\Controllers\TransactionController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [UserController::class, 'login']);
Route::post('register', [UserController::class, 'register']);

Route::group(['middleware' => 'api', 'prefix' => 'auth'], function () {

    Route::post('/signout', [UserController::class, 'signout']);

    Route::get('/trans', [TransactionController::class, 'index']);

    Route::post('/trans', [TransactionController::class, 'store']);

    Route::put('/trans/{id}', [TransactionController::class, 'update']);

    Route::get('/trans/{id}', [TransactionController::class, 'show']);

    Route::delete('/trans/{id}', [TransactionController::class, 'destroy']);
});



